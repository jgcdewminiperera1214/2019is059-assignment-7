#include <stdio.h>
#include <string.h>
int main()
{
   char string[1000];
   int ch = 0, count[26] = {0}, y;

   printf("Enter your string\n");
   gets(string);

   while (string[ch] != '\0') {


      if (string[ch] >= 'a' && string[ch] <= 'z') {
         y = string[ch] - 'a';
         count[y]++;
      }

      ch++;
   }

   for (ch = 0; ch < 26; ch++)
         printf("%c >> %d  \n", ch + 'a', count[ch]);

   return 0;
}

